import by.training.util.HibernateUtil;
import org.hibernate.Session;
import org.sample.entities.Item;
import org.sample.entities.Order;
import org.sample.entities.OrderGood;
import org.sample.entities.User;

import javax.persistence.Query;
import java.util.List;

public class MainTest {
    public static void main(String[] args) {
//users
        User person1 = new User("Alex", "alex");
        User person2 = new User("Victor", "22L");
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();


            session.save(person1);
            session.save(person2);

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        List<User> list = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("FROM User");
            list = (List<User>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (list != null && !list.isEmpty()) {
            for (User person : list) {
                System.out.println(person);
            }
        }
//items
        Item item1 = new Item("Notebook", 5.36);
        Item item2 = new Item("Pen", 0.8);
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();


            session.save(item1);
            session.save(item2);

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        List<Item> goods = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("FROM Item");
            goods = (List<Item>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (list != null && !list.isEmpty()) {
            for (Item item : goods) {
                System.out.println(item);
            }
        }
// orders
        Order order1 = new Order(person1, 5.36);
        Order order2 = new Order(person1, 8.34);
        Order order3 = new Order(person2, 0.0);
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();


            session.save(order1);


            session.save(order2);


            session.save(order3);

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        List<Order> orders = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("FROM Order");
            orders = (List<Order>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (orders != null && !orders.isEmpty()) {
            for (Order item : orders) {
                System.out.println(item);
            }
        }
// orders_goods
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            OrderGood orderG1 = new OrderGood(order1, item1);
            session.save(orderG1);

            OrderGood orderG2 = new OrderGood(order1, item2);
            session.save(orderG2);

            OrderGood orderG3 = new OrderGood(order3, item1);
            session.save(orderG3);

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        List<OrderGood> ordersG = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("FROM OrderGood");
            ordersG = (List<OrderGood>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (ordersG != null && !ordersG.isEmpty()) {
            for (OrderGood orderGood : ordersG) {
                System.out.println(orderGood);
            }
        }
//updates
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("update Order set totalPrice = :total where id = :idParam");
            query.setParameter("idParam", 1L);
            query.setParameter("total", 6.16);

            query.executeUpdate();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();

            Query query = session.createQuery("FROM OrderGood");
            ordersG = (List<OrderGood>) query.getResultList();

            session.getTransaction().commit();
        } catch (Throwable cause) {
            cause.printStackTrace();
        }

        if (ordersG != null && !ordersG.isEmpty()) {
            for (OrderGood orderGood : ordersG) {
                System.out.println(orderGood);
            }
        }
    }
}
