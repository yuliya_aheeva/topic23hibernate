package org.sample.repositories;

import org.sample.entities.Item;
import org.sample.entities.User;

import javax.servlet.http.HttpSession;
import java.sql.*;
import java.util.logging.Logger;

public class ShopRepository {
    private Connection connection;
    private Logger logger = Logger.getLogger(ShopRepository.class.getName());

    public ShopRepository(Connection connection) {
        this.connection = connection;
    }

    public User getUserByName(final String name) {
        try (PreparedStatement st = connection.prepareStatement("SELECT * FROM users WHERE user_name='" + name + "'");
             ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    User x = new User(
                            rs.getLong("ID"),
                            rs.getString("USER_NAME"),
                            rs.getString("PASSWORD"));

                    logger.info("got user by name");
                    return x;
                }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addUser(HttpSession session) {
        try (PreparedStatement st = this.connection.prepareStatement(
                "INSERT INTO users(USER_NAME, PASSWORD) values (?,?)", Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, (String) session.getAttribute("name"));
            st.setString(2, null);
            st.addBatch();
            int affectedRows = st.executeUpdate();
            assert (affectedRows > 0);
            ResultSet generatedKeys = st.getGeneratedKeys();
            generatedKeys.next();
            long id = generatedKeys.getLong(1);

            logger.info("new user added, id: " + id);
            session.setAttribute("userId", id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Item getItemByName(final String name) {
        try (PreparedStatement st = connection.prepareStatement("SELECT * FROM goods WHERE title='" + name + "'");
             ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                Item x = new Item(
                        rs.getLong("ID"),
                        rs.getString("TITLE"),
                        rs.getDouble("PRICE"));

                logger.info("got item by name");
                return x;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addItemToOrder(long orderId, long goodId) {
        try (PreparedStatement st = this.connection.prepareStatement(
                "INSERT INTO orders_goods (order_id, good_id) values (?, ?)")) {
            st.setLong(1, orderId);
            st.setLong(2, goodId);
            st.addBatch();
            st.executeUpdate();

            logger.info("item added");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createOrder(HttpSession session) {
        try (PreparedStatement st = this.connection.prepareStatement(
                "INSERT INTO orders (user_id) values (?)", Statement.RETURN_GENERATED_KEYS)) {
            st.setLong(1, (long) session.getAttribute("userId"));
            st.addBatch();
            int affectedRows = st.executeUpdate();
            assert (affectedRows > 0);
            ResultSet generatedKeys = st.getGeneratedKeys();
            generatedKeys.next();
            long id = generatedKeys.getLong(1);

            logger.info("order added, id: " + id);

            session.setAttribute("orderId", id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void updateOrder(final long orderId, final long userId, final Double total) {
        try (PreparedStatement st = connection.prepareStatement(
                "UPDATE orders SET total_price = ? WHERE id =" + orderId)) {
            st.setDouble(1, total);
            st.executeUpdate();

            logger.info("order updated");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
