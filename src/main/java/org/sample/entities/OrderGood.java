package org.sample.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "ORDERS_GOODS")
public class OrderGood implements Serializable {

    private static final long serialVersionUID = 3906771677381811334L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false, updatable = false)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ORDER_ID")
    private Order orderId;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GOOD_ID")
    private Item goodId;

    public OrderGood(Order orderId, Item goodId) {
        this.orderId = orderId;
        this.goodId = goodId;
    }
}
